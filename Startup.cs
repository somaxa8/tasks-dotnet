using WebApplication1.Configs;
using WebApplication1.Repositories;

namespace WebApplication1;

public class Startup {
        
    public Startup(IConfiguration configuration) {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    public void ConfigureServices(IServiceCollection services) {
        services.AddControllers();
        services.AddDbContext<DatabaseConfig>();
        services.AddScoped<TaskRepository>();
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
        
        if (env.IsDevelopment()) {
            app.UseDeveloperExceptionPage();
        }

        app.UseRouting();

        app.UseEndpoints(endpoints => {
            endpoints.MapControllers();
        });
    }
}

using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Model;

public class Task {
    [Key]
    public int Id { get; set; }
    
    [Required]
    public string? Title { get; set; }
    
    public string? Description { get; set; }
}
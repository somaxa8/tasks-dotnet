using Microsoft.EntityFrameworkCore;

namespace WebApplication1.Configs;

public class DatabaseConfig : DbContext {
    
    protected readonly IConfiguration Configuration;

    public DatabaseConfig(IConfiguration configuration) {
        Configuration = configuration;
    }

    protected override void OnConfiguring(DbContextOptionsBuilder options) {
        // connect to sqlite database
        options.UseSqlite(Configuration.GetConnectionString("connection"));
    }
    
    public DbSet<Model.Task> Tasks { get; set; }
    
}
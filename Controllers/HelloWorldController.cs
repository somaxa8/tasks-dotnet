using Microsoft.AspNetCore.Mvc;
using WebApplication1.Repositories;
using Task = WebApplication1.Model.Task;

namespace WebApplication1.Controllers;

[ApiController]
[Route("/tasks")]
public class HelloWorldController : ControllerBase {

    private readonly TaskRepository _taskRepository;

    public HelloWorldController(TaskRepository taskRepository) {
        _taskRepository = taskRepository;
    }
    
    [HttpGet]
    public OkObjectResult Get() {
        var response = _taskRepository.GetAllAsync();
        return Ok(response);
    }
    
    [HttpGet("{id}")]
    public IActionResult GetById(int id) {
        var response = _taskRepository.GetByIdAsync(id);
        return Ok(response);
    }

    [HttpPost]
    public CreatedAtActionResult Post([FromBody] Task task) {
        var response = _taskRepository.AddAsync(task);
        return CreatedAtAction(nameof(GetById), new {id = response.Id}, response);
    }
    
}
using Microsoft.EntityFrameworkCore;
using WebApplication1.Configs;

namespace WebApplication1.Repositories;

public class TaskRepository : ICrudRepository<Model.Task> {
    private readonly DatabaseConfig _context;
    
    public TaskRepository(DatabaseConfig context) {
        _context = context;
    }

    public async Task<Model.Task> GetByIdAsync(int id) {
        return await _context.Tasks.FindAsync(id);
    }

    public async Task<IEnumerable<Model.Task>> GetAllAsync() {
        return await _context.Tasks.ToListAsync();
    }

    public async Task AddAsync(Model.Task task) {
        await _context.Tasks.AddAsync(task);
        await _context.SaveChangesAsync();
    }

    public Task UpdateAsync(Model.Task entity) {
        throw new NotImplementedException();
    }

    public Task DeleteAsync(Model.Task entity) {
        throw new NotImplementedException();
    }
}